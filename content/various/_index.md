---
title: Various
link: NA
image: /img/organicdevops.webp
description: NA
weight: 10
sitemap:
  priority: 0.6
  weight: 0.5
---
## Links

### Interesting papers
- [Things Could Be Better](https://www.experimental-history.com/p/things-could-be-better)
- [Existential Risk Prevention as Global Priority](https://nickbostrom.com/papers/vulnerable.pdf)
- [My experiment agrees with the theory! - Europhysics News](https://www.europhysicsnews.org/articles/epn/abs/2024/01/epn2024551p29/epn2024551p29.html)

### Report on my internship at IPAG (2019)
- [Studying a pre-stellar core (French)](docs/report-1.pdf)
