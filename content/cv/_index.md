---
title: Curriculum vitae
link: NA
image: /img/organicdevops.webp
description: NA
weight: 10
sitemap:
  priority: 0.6
  weight: 0.5
---

PDF version [here](docs/cv.pdf)