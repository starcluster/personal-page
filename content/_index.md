On this page, you will find content related to my research work at
LPMMC, my teaching at UGA, and more broadly related to physics or
computer science.

{{< social_icons >}}

## Publications

- [Topological photonic band gaps in honeycomb atomic arrays (07/08/2024)](https://scipost.org/SciPostPhysCore.7.3.051)
- [Photonic topological Anderson insulator in a two-dimensional atomic lattice (2023)](https://comptes-rendus.academie-sciences.fr/physique/articles/10.5802/crphys.147/)
- [Topological transitions and Anderson localization of light in disordered atomic arrays (21/04/2022)](https://journals.aps.org/pra/abstract/10.1103/PhysRevA.105.043514)

## Recent Talks
- [Topological photonics in two-dimensional atomic lattices (PhD defense 24/09/2024)](/docs/talks/phd-defense/index.html)
- [Ma thèse en 180 secondes (14/08/2024)](https://www.youtube.com/watch?v=wgSO0h_p6SU)
- [Topology in tight-binding model (07/02/2024)](/docs/talks/topology_tight-binding/talk_2024.html)

## Code
- [PyBott](https://pypi.org/project/PyBott/) is a python package to compute the Bott and spin Bott indices in 2D lattices.
- [Bott in rust](https://gitlab.com/starcluster/bott-in-rust) is a project to compute the Bott index in rust (WIP)



