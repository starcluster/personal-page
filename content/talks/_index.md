---
title: Talks
link: NA
image: 
description: NA
weight: 10
sitemap:
  priority: 0.6
  weight: 0.5
---


Slides of talks I have given:

## 2024
- [Topology in tight-binding models](docs/talks/topology_tight-binding/talk_2024.html)

## 2023
- [Introduction to quantum physics (French)](docs/talks/intro_physique_quantique.pdf)
- [ChatGPT for the physicist](docs/talks/chatGPT_physics_slides.pdf)
- [Topology in photonic graphene](docs/talks/talk_lpmmc_2023.pdf)
- [How to hijack your microwave? (French)](docs/talks/micro-ondes.pdf)

## 2022
- [Light in honeycomb lattice (GDR ondes complexes)](docs/talks/slides_nice_gdr_2022.pdf)

## 2021
- [Command line on steroids](docs/talks/cli_on_steroids.pdf)
- [Internet c'est politique](docs/talks/slides_conf_rezine.pdf)

