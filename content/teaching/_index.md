---
title: Teaching
link: NA
image: /img/teaching.png
description: NA
weight: 10
sitemap:
  priority: 0.6
  weight: 0.5
---

## Algorithmique et programmation fonctionelle inf201

Quelques liens qui peuvent être utiles:

- [OcamlPro](https://try.ocaml.pro/) pour utiliser OCaml en ligne
- [Doc d’OCaml](https://v2.ocaml.org/releases/4.14/htmlman/index.html) la bible sur OCaml, exhaustif mais parfois un peu ardue (anglais)
- [Cours](https://ocaml.gelez.xyz/) d’informatique qui suit le programme d’inf201 (français)

### CM IMA

- [Récursivité dans N (06/02/2024)](docs/inf201/recursivite_dans_N.html)

<div style="display: flex; flex-wrap: wrap; gap: 20px;">

<div style="flex: 1; min-width: 250px;">

### Annales 2024

- [IE1](docs/inf201/IE1c.pdf) [Correction](docs/inf201/IE1c_corrige.ml)
- [IE2](docs/inf201/IE2c.pdf) [Correction](docs/inf201/IE2c_corrige.ml)
- [IE3](docs/inf201/IE3c.pdf) [Correction](docs/inf201/IE3c_corrige.ml)


### Annales 2023

- [IE1](docs/inf201/IE1b.pdf) [Correction](docs/inf201/IE1b_corrige.pdf)
- [IE2](docs/inf201/IE2b.pdf) [Correction](docs/inf201/IE2b_corrige.pdf)
- [IE3](docs/inf201/IE3b.pdf) [Correction](docs/inf201/IE3b_corrige.pdf)
- [IE4](docs/inf201/IE4b.pdf) [Correction](docs/inf201/IE4b_corrige.pdf)
- [Projet 2023](docs/inf201/projet-inf2x1-2023.pdf)


### Annales 2022

- [IE1](docs/inf201/IE1.pdf)
- [IE2](docs/inf201/IE2.pdf) [Correction](docs/inf201/IE2_corrige.pdf)
- [IE3](docs/inf201/IE3.pdf) [Correction](docs/inf201/IE3_corrige.pdf)
- [IE4](docs/inf201/IE4.pdf) [Correction](docs/inf201/IE4_corrige.pdf)

## Algèbre linéaire première année mat201

### Sujets de colle

Voici quelques sujets que j’ai donné en colles en 2022. La plupart des exercices proviennent de [bibmath](https://www.bibmath.net/), du site [exo7](http://exo7.emath.fr/) ou de mon imagination.

- [Systèmes linéaires](docs/mat201/syst_lin.pdf)
- [Espaces vectoriels](docs/mat201/ev.pdf)
- [Applications linéaires](docs/mat201/app_lin.pdf)

