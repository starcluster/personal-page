type point_3d = float*float*float;;

let origine:point_3d = (0.,0.,0.);;

let unite:point_3d = (1.,1.,1.);;

type sphere = float*point_3d;;

let sph1 = (0.8, origine);;

let sph2 = (0.2, origine);;

let sph3 = (0.5, unite);;

type cube = float*point_3d;;

type solide = S of sphere | C of cube;;

let sol1 = S(sph1);;

let pi = 3.14;;

let surface sol = match sol with
  |S(r,_) -> 4.*.pi*.r*.r
  |C(c,_) -> 8.*.c*.c
;;

let volume sol = match sol with
  |S(r,_) -> 4.*.pi*.r*.r*.r/.3.
  |C(c,_) -> c*.c*.c
;;

let distance_3d (p1:point_3d) (p2:point_3d):float =
  let x1,y1,z1 = p1 in
  let x2,y2,z2 = p2 in
  Float.sqrt((x1-.x2)*.(x1-.x2) +. (y1-.y2)*.(y1-.y2) +. (z1-.z2)*.(z1-.z2))
;;

let intersection_sphere s1 s2 =
  let r1,c1 = s1 in
  let r2,c2 = s2 in r1 +. r2 > distance_3d c1 c2 
;;

let inter (a1, a2) (b1, b2) =
    let min_a = min a1 a2 in
    let max_a = max a1 a2 in
    let min_b = min b1 b2 in
    let max_b = max b1 b2 in

    let length =
        if max min_a min_b <= min max_a max_b then
            min max_a max_b -. max min_a min_b
        else
            0.0
    in

    let intersect = max min_a min_b <= min max_a max_b 
    in (intersect, length);;

intersection_sphere sph1 sph2;;
intersection_sphere sph2 sph3;;

inter (1.,3.) (2.,4.);;

inter (2.,4.) (1.,3.);;

inter (2.,10.) (3.,7.);;

let inter_cube (c1,(x1,y1,z1)) (c2,(x2,y2,z2)) =
  let bx,lx = inter (x1,x1+.c1) (x2,x2+.c2) in
  let by,ly = inter (y1,y1+.c1) (y2,y2+.c2) in
  let bz,lz = inter (z1,z1+.c1) (z2,z2+.c2) in
  if bx && by && bz then Some(lx*.ly*.lz)
  else None
;;

let cub1:cube = (1., origine);;

let cub2:cube = (1., unite);;

let cub3:cube = (1., (2.,2.,2.));;

inter_cube cub1 cub3;;
    
  
