let rec lucas p q n =
  match n with
  | 0 -> 0
  | 1 -> 1
  |_ -> p*(lucas p q (n-1)) - q*(lucas p q (n-2))
;;

lucas 1 (-1) 3;;

let rec inserer x l = match l with
  |[] -> [x]
  |t::q -> if x < t then x::t::q else t::(inserer x q);;

let rec tri l = match l with
  |[] -> []
  |t::q -> inserer t (tri q);;

tri [1;4;0;3;5];;

let add_elem x l = List.map (fun y -> (x, y)) l;;

let cartesian l1 l2 =
  List.fold_right (fun x acc -> (add_elem x l2)@acc) l1 [];;

cartesian [1;2] [3;4;5];;

let comp f g = fun x -> g (f x);;

let (>>) f g = comp f g;;

let comp_multi l = List.fold_right (fun f acc -> acc >> f) l (fun x -> x);;
let comp_multi2 = List.fold_left (fun acc f-> f >> acc) (fun x -> x);;

let h2 = comp_multi2 [(fun x -> 2*x); (fun x -> x+1); (fun x -> 3*x)];;

h2 1;;
