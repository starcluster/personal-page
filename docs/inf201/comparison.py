import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

if __name__ == "__main__":
    plt.style.use('dark_background')
    x = np.linspace(1,20,100)
    plt.plot(x, np.log(x),lw=4,color="red",label="$y=\log(x)$")
    plt.plot(x, x,lw=4,color="yellow",label="$y=x$")
    plt.xlabel("",fontsize=20)
    plt.ylabel("",fontsize=20)
    plt.title("",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(fontsize=20)
    plt.savefig("comparison.pdf",format="pdf",bbox_inches='tight')
    plt.savefig("comparison.png",format="png",bbox_inches='tight',dpi=300)
    plt.show()
    
