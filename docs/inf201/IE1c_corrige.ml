let f x y = x^y;;
let f a b c = (a>b)=c;;
let f g x = (g x) + 1;;
let f x = print_int x;;

let expr1 a b = a && (not b);;
let expr2 a b = (a && b) || ((not a) && b);;
let expr3 a b = a || b && a;;
let expr4 a b = (a && b) || ((not a) && (not b));;

let gen_table expr =
  (expr true true, expr true false, expr false true, expr false false)
;;

gen_table expr1;;
gen_table expr2;;
gen_table expr3;;
gen_table expr4;;

'a'^'b';;

(* EXO 4 A || C || not B *)

let sgn (x:float):string =
  if x = 0. then "nul" else if x < 0. then "negatif"
  else "positif";;

let sgn_2 (x:float):string =
  match x with
  | 0. -> "nul"
  | x when x > 0. -> "positif"
  | _-> "negatif"
;;
         
